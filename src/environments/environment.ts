// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDGeg710cdRA96sf3R2h6ApUiFjPh2LFkQ",
    authDomain: "depor-c251e.firebaseapp.com",
    databaseURL: "https://depor-c251e.firebaseio.com",
    projectId: "depor-c251e",
    storageBucket: "depor-c251e.appspot.com",
    messagingSenderId: "321507984123",
    appId: "1:321507984123:web:a40d336d19964fd640e65e",
    
    }  
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

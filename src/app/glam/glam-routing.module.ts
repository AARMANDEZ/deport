import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GlamPage } from './glam.page';

const routes: Routes = [
  {
    path: '',
    component: GlamPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GlamPageRoutingModule {}

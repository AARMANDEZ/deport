import { Component, OnInit } from '@angular/core';
import {JugadoresService} from '..//services/jugadores.service';
import { jugadores } from '../models/jugadores.interface';

@Component({
  selector: 'app-glam',
  templateUrl: './glam.page.html',
  styleUrls: ['./glam.page.scss'],
})
export class GlamPage implements OnInit {
  jugadores: jugadores[];
  constructor(private jugadorService:JugadoresService) { }

  ngOnInit(){
    this.jugadorService.getJugadores().subscribe(res =>{
      this.jugadores=res;
      console.log('Tareas', res);
      
    });
  }

}

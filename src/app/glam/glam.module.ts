import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GlamPageRoutingModule } from './glam-routing.module';

import { GlamPage } from './glam.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GlamPageRoutingModule
  ],
  declarations: [GlamPage]
})
export class GlamPageModule {}

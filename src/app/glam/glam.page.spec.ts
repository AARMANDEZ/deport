import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GlamPage } from './glam.page';

describe('GlamPage', () => {
  let component: GlamPage;
  let fixture: ComponentFixture<GlamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlamPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GlamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

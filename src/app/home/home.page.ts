import { Component, OnInit} from '@angular/core';
import {equipos} from '../models/task.interface';
import {EquiposService} from '..//services/todo.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  equipos: equipos[];
  
  constructor(private equipoService:EquiposService, ) {}
  ngOnInit(){
    this.equipoService.getEquipos().subscribe(res =>{
      this.equipos=res;
      console.log('Tareas', res); 
      
    });
    

    
      
  }
  
  

  

}

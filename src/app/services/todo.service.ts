import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {equipos} from '../models/task.interface';




@Injectable({
  providedIn: 'root'
})
export class EquiposService {
  private equiposCollection: AngularFirestoreCollection<equipos>;
  private equipos: Observable<equipos[]>;
  
  
  constructor(db:AngularFirestore) { 
    this.equiposCollection = db.collection<equipos>('equipos');
    this.equipos = this.equiposCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map(a=>{
          const data =a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ... data};
        });
      }
    ));

    
  }
  getEquipos() {
    return this.equipos;
  }
  getEquipo(id:string) {
    return this.equiposCollection.doc<equipos>(id).valueChanges();
  }
  updateEquipo(todo: equipos, id: string) {
    return this.equiposCollection.doc(id).update(todo);
  }
  addEquipo(todo: equipos) {
    return this.equiposCollection.add(todo);
  }
  removeEquipo(id: string) {
    return this.equiposCollection.doc(id).delete();
  }

  
}


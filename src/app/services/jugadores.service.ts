import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {jugadores} from '../models/jugadores.interface';



@Injectable({
  providedIn: 'root'
})

export class JugadoresService {
  
  private jugadoresCollection: AngularFirestoreCollection<jugadores>;
  private jugadores: Observable<jugadores[]>;
  
  constructor(db:AngularFirestore) { 
   

    this.jugadoresCollection = db.collection<jugadores>('jugadores');// ref=> ref.where('eid', '==',this.equipoId));
    this.jugadores = this.jugadoresCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map(a=>{
          const data =a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ... data};
        });
      }
    ));
  }
  

  getJugadores() {
    return this.jugadores;
  }
  getJugador(id:string) {
    return this.jugadoresCollection.doc<jugadores>(id).valueChanges();
  }
  updateJugador(todo: jugadores, id: string) {
    return this.jugadoresCollection.doc(id).update(todo);
  }
  addJugador(todo: jugadores) {
    return this.jugadoresCollection.add(todo);
  }
  removeJugador(id: string) {
    return this.jugadoresCollection.doc(id).delete();
  }
}




